const User = require("../models/user.js");
const Order = require("../models/order.js");
const Product = require("../models/product.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");


module.exports.checkEmail = (requestBody) => {
	return User.find({email: requestBody.email}).then((result,error) =>{
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				return true
			} else {
				return false
			}
		}
	})
}

module.exports.adminAccess = (requestBody) => {
	return User.findById(userData.id).then((result,error) =>{
		if (result.isAdmin === true) {
			return true
		}else{
			return false
		}
	})
}

module.exports.getAllUsers = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result.isAdmin === false) {
			return "You don't have access to proceed"
		} else {
			return User.find({}).then(result => {
				return result
			})
		}
	})
};

module.exports.getUser = (profile) => {
	console.log(profile.id)
	return User.findById(profile.id).then(result => {
		return result
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 12),
		mobileNo: reqBody.mobileNo,
	})
	return User.findOne({email: reqBody.email}).then(result => {
		if (result !== null) {
			return `This email is already registered`
		} else {
			return newUser.save().then((saved, error) => {
					if (error) {
						console.log(error)
						return false
					} else {
						return true
				}
			})
		}
	})
};

module.exports.adminOnly = (reqParams, userData) => {
	return User.findById(userData.id).then(result => {
		if (result.isAdmin === false) {
			return "You don't have access to make these changes"
		} else {
			return User.findByIdAndUpdate(reqParams.id, {isAdmin: true}).then((result, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return true
				}
			})
		}
	})
};

module.exports.userLogin = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result === null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
};