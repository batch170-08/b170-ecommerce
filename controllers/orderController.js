const User = require("../models/user.js");
const Order = require("../models/order.js");
const Product = require("../models/product.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

module.exports.getAllOrders = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result.isAdmin === false) {
			return "You don't have access to proceed"
		} else {
			return Order.find({}).then(result => {
				return result
			})
		}
	})
};

module.exports.checkOut = (order) => {
	const {userId, productId, quantity} = order;
	return User.findById(userId).then(result => {
		if (result.isAdmin === true) {
			return "An admin account is not allowed to make orders"
		} else {
			return Product.findById(productId).then((result, error) => {
				let newOrder = new Order ({
					userId: userId,
					totalAmount: quantity * result.price,
					itemsOrdered: [{
						productId: productId,
						price: result.price,
						quantity: quantity
					}]
				})
				return newOrder.save().then((result, error) => {
					if (error) {
						return false
					} else {
						return result
					}
				})
			})
		}
	})
};

module.exports.myOrders = (userData) => {
	return User.findById(userData).then(result => {
		if (result.isAdmin === true) {
			return "An admin account is not allowed to make orders"
		} else {
			return Order.findOne({userId: userData}).then(result => {
				if (result === null) {
					return "You don't have an order yet"
				} else {
					return result
				}
			})
		}
	})
}