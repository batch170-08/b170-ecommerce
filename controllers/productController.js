const User = require("../models/user.js");
const Order = require("../models/order.js");
const Product = require("../models/product.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result
	})
};

module.exports.getAvailableProducts = () => {
	return Product.find({isAvailable: true}).then(result => {
		return result
	})
};

module.exports.getProduct = (reqParams) => {
	console.log(reqParams.productId)
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
};

module.exports.addProduct = (reqBody, userData) => {
	return User.findById(userData.id).then(result => {
		if (result.isAdmin === false) {
			return false
		} else {
			let newProduct = new Product ({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
				image: reqBody.image,
				stock: reqBody.stock
			})
			return Product.findOne({name: reqBody.name}).then(result => {
				if (result !== null) {
					return `This product is already in the database`
				} else {
					return newProduct.save().then((result, error) => {
						if (error) {
							console.log(error)
							return false
						} else {
							return true
						}
					})
				}
			})
		}
	})
};

module.exports.updateProduct = (reqParams, reqBody, userData) => {
	return User.findById(userData.id).then(result => {
		if (result.isAdmin === false) {
			return "You don't have access to make these changes"
		} else {
			let updatedProduct = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
				image: reqBody.image,
				stock: reqBody.stock
			}
			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
				if (error) {
					console.log(error)
					return false
				} else {
					return true
				}
			})
		}
	})
}

module.exports.archiveProduct = (reqParams, reqBody, userData) => {
	return User.findById(userData.id).then(result => {
		if (result.isAdmin === false) {
			return "You don't have access to make these changes"
		} else {
			let updatedProduct = {
				isAvailable: false,
				stock: 0
			}
			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
				if (error) {
					console.log(error)
					return false
				} else {
					return true
				}
			})
		}
	})
};