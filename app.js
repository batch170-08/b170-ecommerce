const express = require("express");
const mongoose = require("mongoose");
const cors= require("cors");
const orderRoutes = require("./routes/orderRoutes");
const productRoutes = require("./routes/productRoutes");
const userRoutes = require("./routes/userRoutes");
const app = express();
const port = 4000;

const corsOption = {origin: [
    `http://localhost:3000`,
    `https://acosta-ecommerce-app.vercel.app/`
    ]}
app.use(cors({origin: true}));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://aelacosta:Huskar0!1@wdc028-course-booking.fx063.mongodb.net/b170-Acosta-Ecommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let database = mongoose.connection;

database.on("error", console.error.bind(console, "Connection Error"));

database.once("open", () => console.log("We're connected to the database"));

app.use("/ecommerce/users", userRoutes);
app.use("/ecommerce/orders", orderRoutes);
app.use("/ecommerce/products", productRoutes);

/*app.listen(port, () => console.log(`API now online at port ${port}`));*/

app.listen(process.env.PORT || port, () => console.log(`API now online at port ${process.env.PORT || port}`));