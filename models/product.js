const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, `Product name is required`]
		},
		description: {
			type: String,
			required: [true, `Product description is required`]
		},
		price: {
			type: Number,
			required: [true, "Price for the product is required"]
		},
		image: {
			type: String,
			required: [true, "Image of the product is required"]
		},
		isAvailable: {
			type: Boolean,
			default: true
		},
		stock: {
			type: Number,
			default: 0,
			required: [true, `Out of stock`]
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	}
);

module.exports = mongoose.model("Product", productSchema);