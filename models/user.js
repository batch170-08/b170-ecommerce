const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, `First name is required`]
		},
		lastName: {
			type: String,
			required: [true, `Last name is required`]
		},
		email: {
			type: String,
			required: [true, `Email address is required`]
		},
		password: {
			type: String,
			required: [true, `Password is required`]
		},
		mobileNo: {
			type: String,
			required: [true, `Please provide a mobile number`]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		orders: [
			{
				orderId: {
					type: String,
					required: [true, `Product ID is required`]
				},
				orderedOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: `To Ship`
				}
			}
		]
	}
);

module.exports = mongoose.model("User", userSchema);