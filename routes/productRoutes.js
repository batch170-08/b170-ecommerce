const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const productController = require("../controllers/productController.js")
const userController = require("../controllers/userController.js")
const orderController = require("../controllers/orderController.js")

//Retrieve all products
router.get("/", (req, res) => {
	productController.getAllProducts().then(result => res.send(result))
});

//Retrieve all active products
router.get("/available", (req,res) => {
	productController.getAvailableProducts().then(result => res.send(result))
});


//Retrieve single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId)
	productController.getProduct(req.params).then(result => res.send(result))
});

//Create product
router.post("/add", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization)
	productController.addProduct(req.body, userData).then(result => res.send(result))
})


//Update product
router.put("/:productId/updata", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization)
	productController.updateProduct(req.params, req.body, userData).then(result => res.send(result))
});

//Archive Product
router.put("/:productId/archive", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization)
	productController.archiveProduct(req.params, req.body, userData).then(result => res.send(result))
})


module.exports = router;