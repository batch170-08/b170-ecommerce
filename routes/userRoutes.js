const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

//check Email
router.post("/check", (req, res) =>{
	userController.checkEmail(req.body).then(result => res.send(result))
});


//Retrieve all user
router.get("/", auth.verifyToken, (req,res) => {
	const userData = auth.decodeToken(req.headers.authorization)
	userController.getAllUsers(userData).then(result =>
		res.send(result))
});

//Retrieve a user
router.get("/profile", auth.verifyToken, (req, res) => {
	const profile = auth.decodeToken(req.headers.authorization)
	userController.getUser(profile).then(result => res.send(result))
});

// User registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});

//Set user as admin
router.put("/:userId/set-as-admin", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization)
	userController.adminOnly(req.params, userData).then(
		result => res.send(result))
})

//check if admin
router.post("/admin-access", (req, res) =>{
	userController.adminAccess(req.body).then(result => res.send(result))
});

//User login / authentication
router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(result => res.send(result))
})


module.exports = router;