const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const orderController = require("../controllers/orderController.js")
const productController = require("../controllers/productController.js")
const userController = require("../controllers/userController.js")

//Retrieve All Order
router.get("/", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization)
	orderController.getAllOrders(userData).then(result => res.send(result))
});

//Create Order
router.post("/checkout", auth.verifyToken, (req, res) => {
	let order = {
		userId: auth.decodeToken(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	orderController.checkOut(order).then(result => res.send(result))
});


//Get user's orders
router.get("/my-orders", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization).id
	orderController.myOrders(userData).then(result => res.send(result))
})

module.exports = router;